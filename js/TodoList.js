const TodoList = function(loadDummyData) {
  this.list = [];
  if (loadDummyData || false) {
    this.list = [
      {
        id: this.getUUID(),
        todo: "Clean my car",
        checked: true
      },
      {
        id: this.getUUID(),
        todo: "Play with Javascript",
        checked: true
      },
      {
        id: this.getUUID(),
        todo: "Help wife with household",
        checked: true
      }
    ];
    this.updateUI();
  }
};

// SHARED METHODS AMONG ALL TodoList instances
TodoList.prototype.add = function(todo) {
  this.list.unshift(todo);
  this.updateUI();
};

TodoList.prototype.delete = function(id) {
  this.list = this.list.filter(el => el.id !== id);
  this.updateUI();
};

TodoList.prototype.check = function(id) {
  this.list = this.list.map(el => {
    if (el.id === id) {
      el.checked = !el.checked;
    }
    return el;
  });
  this.updateUI();
};

TodoList.prototype.updateUI = function() {
  const ulReference = document.getElementsByClassName("todoApp__list")[0]; //ref to UL
  const todoTemplate = `<li class="todoApp__list__item%todocheck%" data-id="%myID%">
                              <span class="todoApp__list__item__text">%todotext%</span>
                              <a href="#" class="icon icon--remove"></a>
                              <a href="#" class="icon icon--circular icon--check"></a>
                          </li>`; // dummy li

  let allHtml = "";
  this.list.forEach(function(el) {
    let temp = todoTemplate
      .replace("%todotext%", el.todo)
      .replace("%myID%", el.id);
    if (el.checked) {
      temp = temp.replace("%todocheck%", " todoApp__list__item--checked");
    } else {
      temp = temp.replace("%todocheck%", "");
    }
    allHtml += temp;
  });

  ulReference.innerHTML = allHtml;
};

TodoList.prototype.getUUID = function() {
  return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
};
