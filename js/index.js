const myTodoApplication = new TodoList(true);

// DOM REFERENCES
const btn = document.getElementsByClassName("todoApp__form__button")[0];
const input = document.getElementsByClassName("todoApp__form__input")[0];
const form = document.getElementsByClassName("todoApp__form")[0];
const ul = document.getElementsByClassName("todoApp__list")[0];

// EVENTS
btn.addEventListener("click", function(evt) {
  evt.preventDefault();
  add();
});

form.addEventListener("submit", function(evt) {
  evt.preventDefault();
  add();
});

ul.addEventListener("click", function(evt) {
  if (evt.target.classList.contains("icon--check")) {
    myTodoApplication.check(evt.target.parentElement.dataset.id);
  }
  if (evt.target.classList.contains("icon--remove")) {
    myTodoApplication.delete(evt.target.parentElement.dataset.id);
  }
});

function add() {
  if (input.value != "") {
    input.classList.remove("todoApp__form__input--error");
    myTodoApplication.add({
      id: myTodoApplication.getUUID(),
      todo: input.value,
      checked: false
    });
    input.value = "";
    input.focus();
  } else {
    input.classList.add("todoApp__form__input--error");
  }
}
